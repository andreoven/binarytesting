import CartParser from './CartParser';
import uuid from 'uuid/v4';


let parser;

beforeEach(() => {
	parser = new CartParser();
});

describe('CartParser - unit tests', () => {
	// Add your unit tests here.
	it('should return error if path to the file is wrong or no file exists', function () {
		expect(() => {
			parser.readFile('samples/cart11.csv');
		}).toThrow();
	});

	it('should return empty array without errors if schema of CSV file is valid', function () {
		expect(parser.validate('Product name,Price,Quantity\n Mollis consequat,9.00,2')).toEqual([]);
	});

	it('should return error if header is wrong', function () {
		expect(parser.validate('Product name,Weight,Quantity\nMollis consequat,9.00,2')).toEqual(expect.arrayContaining([expect.objectContaining({"column": 1, "message": "Expected header to be named \"Price\" but received Weight.", "row": 0, "type": "header"})]));

	});

	it('should return error if row receive wrong number of cells', function () {
		expect(parser.validate('Product name,Price,Quantity\nMollis consequat,9.00')).toEqual(expect.arrayContaining([expect.objectContaining({ type: 'row', row: 1, column: -1, message: 'Expected row to have 3 cells but received 2.' })]));
	});

	it('should return error if cell receive nonempty string', function () {
		expect(parser.validate('Product name,Price,Quantity\n,9.00,2')).toEqual(expect.arrayContaining([expect.objectContaining({ type: 'cell', row: 1, column: 0, message: 'Expected cell to be a nonempty string but received "".' })]));
	});

	it('should return error if cell is not a positive number', function () {
		expect(parser.validate('Product name,Price,Quantity\n,Mollis consequat,9.00,-2')).toEqual(expect.arrayContaining([expect.objectContaining({ type: 'cell', row: 1, column: 1, message: 'Expected cell to be a positive number but received "Mollis consequat".' })]));
	});

	it('should converts a line to a JSON object and add an `id` property with a uuid as a value to the item', function () {
		expect(parser.parseLine('Mollis consequat,9.00,2')).toEqual({
			name: 'Mollis consequat',
			price: 9,
			quantity: 2,
			id: expect.stringMatching(/[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}/)
		})
	});

	it('should return total price of items', function () {
		const allItems = [
			{
				"id": "3e6def17-5e87-4f27-b6b8-ae78948523a9",
				"name": "Mollis consequat",
				"price": 9,
				"quantity": 2
			},
			{
				"id": "90cd22aa-8bcf-4510-a18d-ec14656d1f6a",
				"name": "Tvoluptatem",
				"price": 10.32,
				"quantity": 1
			},
			{
				"id": "33c14844-8cae-4acd-91ed-6209a6c0bc31",
				"name": "Scelerisque lacinia",
				"price": 18.9,
				"quantity": 1
			},
			{
				"id": "f089a251-a563-46ef-b27b-5c9f6dd0afd3",
				"name": "Consectetur adipiscing",
				"price": 28.72,
				"quantity": 10
			},
			{
				"id": "0d1cbe5e-3de6-4f6a-9c53-bab32c168fbf",
				"name": "Condimentum aliquet",
				"price": 13.9,
				"quantity": 1
			}
		];
		const total = 348.32;
		expect(parser.calcTotal(allItems)).toBeCloseTo(total);
	});

	it('should return error object with given values', function () {
		expect(parser.createError('cell', 1, 1, 'error message')).toEqual({
			type: 'cell',
			row: 1,
			column: 1,
			message: 'error message'
		});
	});
});

describe('CartParser - integration test', () => {
	// Add your integration test here.
	it('should return correct json file after parsing', function () {
		const cartJson = {
			"items": [
				{
					"id": expect.stringMatching(/[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}/),
					"name": "Mollis consequat",
					"price": 9,
					"quantity": 2
				},
				{
					"id": expect.stringMatching(/[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}/),
					"name": "Tvoluptatem",
					"price": 10.32,
					"quantity": 1
				},
				{
					"id": expect.stringMatching(/[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}/),
					"name": "Scelerisque lacinia",
					"price": 18.9,
					"quantity": 1
				},
				{
					"id": expect.stringMatching(/[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}/),
					"name": "Consectetur adipiscing",
					"price": 28.72,
					"quantity": 10
				},
				{
					"id": expect.stringMatching(/[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}/),
					"name": "Condimentum aliquet",
					"price": 13.9,
					"quantity": 1
				}
			],
			"total": 348.32
		};
		expect(parser.parse('samples/cart.csv')).toEqual(cartJson);
	});
});